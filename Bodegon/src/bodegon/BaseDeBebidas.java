
package bodegon;

import java.io.Serializable;
import java.util.ArrayList;


public class BaseDeBebidas implements Serializable  {
       private static BaseDeBebidas instancia;
       private ArrayList<Bebida> bebidas;

    public BaseDeBebidas() {
        //bebidas=new ArrayList<>();
    }
    
    
    private synchronized static void crearInstancia() {
        if(instancia==null){
             instancia=new BaseDeBebidas();
        
        }
    }
    
    public static BaseDeBebidas getInstancia(){
        crearInstancia();
        
       return instancia;
    }
    //SETTERS
    public void setBebidas(ArrayList<Bebida> bebidas) {
        this.bebidas = bebidas;
    }
    public void agregarBebida(String nombre){
        Bebida bebida=new Bebida();
        bebida.setCodigo(bebidas.size());
        bebida.setNombre(nombre);
        bebidas.add(bebida);
                
        
    }
    
    //GETTERS
    public ArrayList<Bebida> getBebidas() {
        return bebidas;
    }
    
    public Bebida obtenerBebida(int codigo){
        
        return bebidas.get(codigo);
    }
    
    
    public String obtenerListadoBebidas(){
        String listado="";
        
        for(Bebida b : bebidas){
           listado+="\n COD["+b.getCodigo() +"]"+b.getNombre()+ " $"+b.getValor()+" PEDIDOS["+b.getPedidos()+"]";
           
        }
        
        return listado;
    
    }
    
    public boolean tienePrecio(Bebida b){
    return b.getValor()!=0;
    }
    

    
}
