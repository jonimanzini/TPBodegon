package bodegon;


import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EntradaSalida {

    public static char leerChar(String texto) { // pide un texto para mostrar en input
        String st = JOptionPane.showInputDialog(texto);
        return (st == null || st.length() == 0 ? '0' : st.charAt(0));
    }

    public static String leerString(String texto) { // pide un texto para mostrar en input
        String st = JOptionPane.showInputDialog(texto);
        return (st == null ? "" : st);
    }

    public static int leerInt(String texto) { // pide un texto para mostrar en input
        int val = -1;
        boolean valido;
        do {
            String st = JOptionPane.showInputDialog(texto);
            try {
                val = Integer.parseInt(st);
                valido = true;
            } catch (NumberFormatException ex) {
                mostrarString("Ingrese un entero!!!\n(En lugar de " + st + ")"); // mensaje en caso de error
                valido = false;
            }
        } while (!valido);
        return val;
    }

    public static boolean leerBoolean(String texto) {  // pide un texto para mostrar en input
        int i = JOptionPane.showConfirmDialog(null, texto, "Consulta", JOptionPane.YES_NO_OPTION);
        return i == JOptionPane.YES_OPTION;
    }

    public static void mostrarString(String s) {  // pide un texto para mostrar en pantalla
        JOptionPane.showMessageDialog(null, s);
    }

    public static String leerPassword(String texto) { // pide un texto para mostrar en pantalla
        final JPasswordField pwd = new JPasswordField();
        ActionListener al;
        al = new ActionListener() { // hace que muestre puntos  en ves de caracteres
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                pwd.requestFocusInWindow();
            }
        };
        Timer timer = new Timer(200, al);
        timer.setRepeats(false);
        timer.start();
        Object[] objs = {texto, pwd};
        String password = "";
        if (JOptionPane.showConfirmDialog(null, objs, "Entrada",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {
            password = String.valueOf(pwd.getPassword());
        }
        return password;
        
        
    }
    
  public static int leerIntEntre(int valor1,int valor2,String texto){
            int valor;
            valor=leerInt(texto);
      
            while(valor<valor1 && valor>valor2 ){
            valor=leerInt("EL VALOR INGRESADO DEBE ESTAR ENTRE "+valor1 +" y "+valor2);
            
            }

          return valor;
      }  
  
  public static float leerFloat(String texto){
      boolean valido=false;
      float numero=-1;
      do{
            try{    
                numero= Float.parseFloat(leerString(texto));
                valido=true;
           }catch(NumberFormatException ex){
                mostrarString("Debe ingresar un float!");
                valido=false;
           }
           }while(!valido);
      return numero;
  }
    
 } 
