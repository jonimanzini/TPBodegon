
package bodegon;

import java.io.IOException;

public class Main {

  
    public static void main(String[] args) {
        String archivoDatos="bodegon.dat";
        Sistema s= new Sistema();
        
        try {
            s = s.deSerializar(archivoDatos);
        } catch (IOException | ClassNotFoundException ex) {
            s.inicializar();
        } finally {
            s.iniciarSesion();
        }

        try {
            s.serializar(archivoDatos);
        } catch (IOException ex) {
            EntradaSalida.mostrarString(ex.getMessage());
        }
    }
    
}
