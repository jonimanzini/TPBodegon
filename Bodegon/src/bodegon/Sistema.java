
package bodegon;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Sistema implements Serializable {
    private ArrayList<Usuario> usuariosList;
    private ArrayList<Preparacion> preparacionesList;
    private ArrayList<Bebida> bebidasList;
    private ArrayList<Comanda> comandasList;

    
    public Sistema() {
        usuariosList=new ArrayList<>();
        preparacionesList=new ArrayList<>();
        bebidasList=new ArrayList<>();
        comandasList=new ArrayList<>();
           
    }
    
    //SERIALIZAR_________________________________________________________
    
     public Sistema deSerializar(String archivo) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(archivo);
        ObjectInputStream o = new ObjectInputStream(f);
        Sistema s = (Sistema) o.readObject();
        return s;
    }

    public void serializar(String archivo) throws IOException {
        FileOutputStream f = new FileOutputStream(archivo);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
    }
    
    //____________________________________________FUNCIONES
    
    
    public void iniciarSesion(){     
        BaseDeUsuarios baseUsuarios=BaseDeUsuarios.getInstancia();
        baseUsuarios.setUsuarios(usuariosList);
        BaseMenu baseMenu=BaseMenu.getInstancia();
        baseMenu.setPreparaciones(preparacionesList);
        BaseDeBebidas baseBebidas=BaseDeBebidas.getInstancia();
        baseBebidas.setBebidas(bebidasList);
        BaseDeComandas baseComandas=BaseDeComandas.getInstancia();
        baseComandas.setComandas(comandasList);
        
        Usuario usuario=null;
        String user=null;
        String pass=null;
        
        boolean corriendo=true;  
        while(corriendo){
           corriendo=EntradaSalida.leerBoolean("DESEA INGRESAR AL SISTEMA ?");
           if(corriendo){ 
                   user=EntradaSalida.leerString("INGRESAR USUARIO");
                   pass=EntradaSalida.leerPassword("INGRESAR PASSWORD");
                for( Usuario u: baseUsuarios.getUsuarios()){
                    if(u.getNombre().equals(user) && u.getContraseña().equals(pass)){
                        usuario = u;
                    }
                }
                
                if(usuario!=null){
                    usuario.proceder();
                }
                else
                {
                EntradaSalida.mostrarString("EL USUARIO ES INCORRECTO");
                }
            }
        }        
    }
    
    public void inicializar(){
        BaseDeUsuarios baseUsuarios=BaseDeUsuarios.getInstancia();
        baseUsuarios.setUsuarios(usuariosList);
        BaseMenu baseMenu=BaseMenu.getInstancia();
        baseMenu.setPreparaciones(preparacionesList);
        BaseDeBebidas baseBebidas=BaseDeBebidas.getInstancia();
        baseBebidas.setBebidas(bebidasList);
        BaseDeComandas baseComandas=BaseDeComandas.getInstancia();
        baseComandas.setComandas(comandasList);
        
        UsuariosFactory f=UsuariosFactory.getInstancia();
        EntradaSalida.mostrarString("CREACION DE USUARIO ADMINISTRADOR");
        baseUsuarios.agregarUsuario(f.crearUsuario("Administrador"));
        
    }
    
}
