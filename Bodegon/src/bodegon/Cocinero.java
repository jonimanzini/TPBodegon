
package bodegon;

public class Cocinero extends Usuario{
    private BaseMenu baseMenu;
    
    public Cocinero() {
        baseMenu=BaseMenu.getInstancia();
        
    }
    
        @Override
    public void proceder(){
        int op;
       do{
            op=EntradaSalida.leerIntEntre(0, 2, "COCINERO: "+getNombre()
                    +"\n1- Cargar Menu"
                    + "\n2- Ver Menues"
                    + "\n0-Salir"); 
            switch(op){
                case 1:cargarMenu();
                break;
                case 2:verMenues();
                break;
            }
       }while(op!=0);
        
    }
    public void cargarMenu(){
        String descripcion=EntradaSalida.leerString("AGREGAR DESCRIPCION DEL MENU");
        baseMenu.agregarPreparacion(descripcion);
        
    }
     public void verMenues(){
        EntradaSalida.mostrarString(baseMenu.obtenerListado());
        
    }
    
}
