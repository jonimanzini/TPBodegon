
package bodegon;

import java.io.Serializable;
import java.util.ArrayList;


public class BaseDeComandas implements Serializable  {
        private static BaseDeComandas instancia;
        private ArrayList<Comanda> comandas;

    public BaseDeComandas() {
        //comandas=new ArrayList<>();
    }
    
    
    private synchronized static void crearInstancia() {
        if(instancia==null){
             instancia=new BaseDeComandas();
        
        }
    }
    
    public static BaseDeComandas getInstancia(){
        crearInstancia();
        
       return instancia;
    } 
    //SETTERS
    public void setComandas(ArrayList<Comanda> comandas) {
        this.comandas = comandas;
    }
    public void agregarComanda(Preparacion p,Bebida b,int mesa){
       Comanda c=new Comanda();
       c.setP(p);c.setB(b);c.setValorTotal();
       c.setMesa(mesa);
       c.setCodigo(comandas.size());
       comandas.add(c);
    }
    //GETTERS
    public ArrayList<Comanda> getComandas() {
        return comandas;
    }
    
    public String obtenerListadoComandas(){
        String listado="";
        
        for(Comanda c : comandas){
           //listado+="\n COD["+c.getCodigo() +"]"+"Preparacion: "+c.getP().getDescripcion()+ " $"+c.getP().getValor()+" Bebida:"+c.getB().getNombre()+" $"+c.getB().getValor();
           listado+="\n"+c.toString();
        }
        
        return listado;
        
    }
    
}
