
package bodegon;

import java.io.Serializable;

public class Bebida implements Serializable  {
    private String nombre;
    private float valor;
    private int pedidos;
    private int codigo;
    public Bebida() {
    }
               //SETTERS -----------------------------------------------
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public void setPedidos(int pedidos) {
        this.pedidos = pedidos;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
            //GETTERS -----------------------------------------------
    public String getNombre() {
        return nombre;
    }

    public float getValor() {
        return valor;
    }

    public int getPedidos() {
        return pedidos;
    }

    public int getCodigo() {
        return codigo;
    }

    
    
}
