
package bodegon;


public class Camarero extends Usuario{
    private int pedidos;
    private BaseDeBebidas baseBebidas;
    private BaseMenu baseMenu;
    private BaseDeComandas baseComandas;
    
    public Camarero() {
        baseBebidas=BaseDeBebidas.getInstancia();
        baseMenu=BaseMenu.getInstancia();
        baseComandas=BaseDeComandas.getInstancia();
    }

    public void setPedidos(int pedidos) {
        this.pedidos = pedidos;
    }

    public int getPedidos() {
        return pedidos;
    }
    
    
    
    @Override
    public void proceder(){
        int op;
        do{
         op=EntradaSalida.leerIntEntre(0, 2, "CAMARERO: "+getNombre()
                +"\n1- Cargar comanda"
                +"\n2- Ver comandas"
                +"\n0-Salir"); 
        switch(op){
            case 1:cargarComanda();
            break;
            case 2:verComandas();
            break;
        }
        }while(op!=0);
        
    }
    
    public void cargarComanda(){
        int op;
        Bebida b=null;
        Preparacion p=null;
        do{
            op=EntradaSalida.leerIntEntre(0, 4,"CAMARERO:"+getNombre()
                    + "\n1-Cargar preparacion"
                    + "\n2-Cargar Bebida"
                    + "\n3-Ver comanda"
                    + "\n4-Finalizar comanda"
                    + "\n0-Salir");
            switch(op){
                case 1:p=cargarPreparacion();
                break;
                case 2:b=cargarBebida();
                break;
                case 3:verComanda(p,b);
                break;
                case 4:finalizarComanda(p,b);
                break;
            
            }
        
        }while(op!=0);
        
    }
    
    public Preparacion cargarPreparacion(){
        int codigo;
        int posicion;
        Preparacion p=null;
        posicion=baseMenu.getPreparaciones().size();
        codigo=EntradaSalida.leerIntEntre(0,posicion,"MENU: "+baseMenu.obtenerListado()+"\n INGRESAR CODIGO ");
        p=baseMenu.obtenerPreparacion(codigo);
        if(baseMenu.tienePrecio(p)){
            EntradaSalida.mostrarString("PEDIDO REALIZADO");
            return p;
        
        }else{
            EntradaSalida.mostrarString("NO SE PUEDE REALIZAR EL PEDIDO YA QUE NO TIENE PRECIO");
            return null;
        }
        
    }
    
    public Bebida cargarBebida(){
        int codigo;
        int posicion;
        Bebida b;
        posicion=baseBebidas.getBebidas().size();
        codigo=EntradaSalida.leerIntEntre(0,posicion,"MENU: "+baseBebidas.obtenerListadoBebidas()+"\n INGRESAR CODIGO ");
        b=baseBebidas.obtenerBebida(codigo);
        if(baseBebidas.tienePrecio(b)){
            EntradaSalida.mostrarString("PEDIDO REALIZADO");
            return b;
        }
        else{
            EntradaSalida.mostrarString("NO SE PUEDE REALIZAR EL PEDIDO YA QUE NO TIENE PRECIO");
            return null;
        }
    }
    
    public void verComanda(Preparacion p,Bebida b){
        if(p==null||b==null){
            EntradaSalida.mostrarString("FALTAN CARGAR DATOS");
        }else{
        EntradaSalida.mostrarString("Comida: "+p.getDescripcion() + " Precio: $"+p.getValor()
        +"\nBebida: "+b.getNombre()+" Precio: $"+b.getValor());
        }
    
    }
    
    public void finalizarComanda(Preparacion p,Bebida b){
        String error=(p==null)?"Preparacion":"Bebida";
        
        if(p!=null && b!= null){
           int posicion;
           int mesa=EntradaSalida.leerInt("Ingresar numero de Mesa:");
           baseComandas.agregarComanda(p, b,mesa);
           posicion=baseComandas.getComandas().size()-1;
           EntradaSalida.mostrarString("Comanda cargada: "+baseComandas.getComandas().get(posicion).toString());
           baseBebidas.obtenerBebida(b.getCodigo()).setPedidos(+1);
           baseMenu.obtenerPreparacion(p.getCod()).setPedidos(+1);
           pedidos+=1;
           
        }
        else
        {
            EntradaSalida.mostrarString("No se puede finalizar la comanda ya que la "+error+" no fue cargada");
        }
    
    }
    
    public void verComandas(){
        EntradaSalida.mostrarString(baseComandas.obtenerListadoComandas());
    
    }
}
