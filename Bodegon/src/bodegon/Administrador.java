    
package bodegon;

import java.util.ArrayList;


public class Administrador extends Usuario{
    private BaseDeUsuarios baseUsuarios;
    private BaseMenu baseMenu;
    private BaseDeBebidas baseBebidas;
    
    
    public Administrador() {
       baseUsuarios=BaseDeUsuarios.getInstancia();
       baseMenu=BaseMenu.getInstancia();
       baseBebidas=BaseDeBebidas.getInstancia();
        
    }
    
    
    
    @Override
    public void proceder(){
        int op;
        do{
        op=EntradaSalida.leerIntEntre(0, 6,"ADMINISTRADOR:"
                + "\n1-Alta usuario"
                + "\n2-Ver usuarios"
                + "\n3-Alta bebida"
                + "\n4-Ver bebidas"
                + "\n5-Ver Menues"
                + "\n6-Asignar Valor"
                + "\n7-Recaudacion Total"
                + "\n8-Preparacion mas pedida"
                + "\n9-Bebida mas pedida"
                + "\n10-Camarero con mas pedidos"
                + "\n0-Salir ");
        
        switch(op){
            case 1:
                    altaUsuario();
            break;
            case 2: 
                    verUsuarios();
            break;
            case 3:
                    altaBebida();
            break;
            case 4:
                    verBebidas();
            break;
            case 5:   
                    verMenues();
            break;
            case 6:
                    asignarValor();
            break;
            case 7:
                    recaudacionTotal();
            break;
            case 8:
                    preparacionMasPedida();
            break;
            case 9:
                    bebidaMasPedida();
            break;
            case 10:
                    camareroMasPedidos();
            break;
            case 0: EntradaSalida.mostrarString(getNombre()+ " salio del sistema");
            break;
        
        }
        
        }while(op!=0);
          
    }
    
    public void altaUsuario(){
        UsuariosFactory f=UsuariosFactory.getInstancia();
        int op;
        op=EntradaSalida.leerIntEntre(0,2 , "ALTA USUARIO:"
                + "\n1- Cocinero"
                + "\n2- Camarero"
                + "\n0- Salir");
        switch(op){
            case 1:baseUsuarios.agregarUsuario(f.crearUsuario("Cocinero"));
                
            break;
            case 2:baseUsuarios.agregarUsuario(f.crearUsuario("Camarero"));
                
            break;
            case 0:
                
            break;
        
        
        }      
    
    }
    
       public void verUsuarios(){
            EntradaSalida.mostrarString("LISTA DE USUARIOS: \n"+baseUsuarios.obtenerListadoUsuarios());
        
        }
       
       public void altaBebida(){
            baseBebidas.agregarBebida(EntradaSalida.leerString("BEBIDAS:"+baseBebidas.obtenerListadoBebidas()+"\nINGRESAR NOMBRE DE BEBIDA "));
           
        
       }
       public void verBebidas(){
            EntradaSalida.mostrarString("MENU DE BEBIDAS:"+baseBebidas.obtenerListadoBebidas());
        }
       
       public void verMenues(){
            EntradaSalida.mostrarString(baseMenu.obtenerListado());         
       }
       
        public void asignarValor(){
            int op,codigo;
            
            do{
                op=EntradaSalida.leerIntEntre(0, 2,"Asignar precio:"
                        + "\n1-Preparacion"
                        + "\n2-Bebida"
                        + "\n0-Salir");
                switch(op){
                    case 1:
                        if(!baseMenu.getPreparaciones().isEmpty()){
                        codigo=EntradaSalida.leerIntEntre(0,baseMenu.getPreparaciones().size(),
                            "INGRESAR CODIGO: "
                                    + "\n"+baseMenu.obtenerListado());
                        baseMenu.obtenerPreparacion(codigo).setValor(EntradaSalida.leerFloat("Ingresar precio"));
                        }
                        else
                        {EntradaSalida.mostrarString("No hay datos cargados");}
                    break;
                    case 2:
                        if(!baseBebidas.getBebidas().isEmpty()){
                        codigo=EntradaSalida.leerIntEntre(0,baseBebidas.getBebidas().size(),
                            "INGRESAR CODIGO:"
                                    + "\n"+baseBebidas.obtenerListadoBebidas());
                         baseBebidas.obtenerBebida(codigo).setValor(EntradaSalida.leerFloat("Ingresar precio"));
                        }
                        else
                        {EntradaSalida.mostrarString("No hay datos cargados");
                        }
                    break;
                }
            }while(op!=0);
        
        
        }
        
        public void recaudacionTotal(){
            float total=0;
            BaseDeComandas baseComandas=BaseDeComandas.getInstancia();
            
            
            for(Comanda c: baseComandas.getComandas()){
                total+=c.getValorTotal();
            
            }
            
            EntradaSalida.mostrarString("La recaudacion total es de: $"+total);
        }
        
        public void preparacionMasPedida(){
            int pedidos=0;
            String preparacion="";
            
            for(Preparacion p: baseMenu.getPreparaciones()){
                if(pedidos<=p.getPedidos()){
                    pedidos=p.getPedidos();
                    preparacion=p.getDescripcion();
                }
                
            }
            
            EntradaSalida.mostrarString("La preparacion mas pedidas es:\n"+preparacion
                    +"\n Pedidos realizados:"+pedidos);
        
        }
        
        
        
        public void bebidaMasPedida(){
            int pedidos=0;
            String bebida="";
            
            for(Bebida b: baseBebidas.getBebidas()){
                if(pedidos<=b.getPedidos());{
                    pedidos=b.getPedidos();
                    bebida=b.getNombre();
                }
                
            }
            
            EntradaSalida.mostrarString(" La bebida mas pedida: "+bebida+"\nSe pidio:"+pedidos);
        
        }
        
        public void camareroMasPedidos(){
            int pedidos=0;
            String nombre="Camarero";
            String camarero="";
            ArrayList<Camarero> camareros=new ArrayList<>();
            
            
            for(Usuario c: baseUsuarios.getUsuarios()){
                if(c.getClass().getClass().getSimpleName().equals(nombre)){
                    camareros.add((Camarero)c);
                
                }
            }
            
            for(Camarero a: camareros){
               if(pedidos<=a.getPedidos()){
                   pedidos=a.getPedidos();
                   camarero=a.getNombre();
               }
            
            }
            EntradaSalida.mostrarString("CAMARERO:"+camarero
            +"\n Mayor cantidad de pedidos"+pedidos);
        
        }
        
    
    
}
