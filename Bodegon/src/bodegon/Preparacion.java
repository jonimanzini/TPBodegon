
package bodegon;

import java.io.Serializable;

public class Preparacion implements Serializable  {
    private String descripcion;
    private float valor;
    private int pedidos;
    private int codigo;
    public Preparacion() {
    }
           
    //SETTERS -----------------------------------------------
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
           
    public void setValor(float valor) {
        this.valor = valor;
    }

    public void setPedidos(int pedidos) {
        this.pedidos = pedidos;
    }

    public void setCod(int cod) {
        this.codigo = cod;
    }
    
            //GETTERS -----------------------------------------------
    public String getDescripcion() {
        return descripcion;
    }

    public float getValor() {
        return valor;
    }

    public int getPedidos() {
        return pedidos;
    }

    public int getCod() {
        return codigo;
    }
    
    
    
}
