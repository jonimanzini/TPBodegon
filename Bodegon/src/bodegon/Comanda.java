
package bodegon;

import java.io.Serializable;


public class Comanda implements Serializable  {
    private Preparacion p;
    private Bebida b;
    private int mesa;
    private float valorTotal;
    private int codigo;

    public Comanda() {
    }
    //SETTERS -----------------------------------------------
    public void setP(Preparacion p) {
        this.p = p;
    }

    public void setB(Bebida b) {
        this.b = b;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

    public void setValorTotal() {
        this.valorTotal = p.getValor()+b.getValor();
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    //GETTERS -----------------------------------------------
    public Preparacion getP() {
        return p;
    }

    public Bebida getB() {
        return b;
    }

    public int getMesa() {
        return mesa;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public int getCodigo() {
        return codigo;
    }
    public String toString(){
        String texto; 
        texto="COD ["+codigo+"]"+"Preparacion:[ "+p.getDescripcion()+"] $"+p.getValor()+" Bebida: ["+b.getNombre()+"] $"+b.getValor()+" total: $"+valorTotal;
        return texto;  
    }
    
    
}
