
package bodegon;

import java.io.Serializable;
import java.util.ArrayList;

public class BaseMenu implements Serializable  {
    private static BaseMenu instancia;
    private ArrayList<Preparacion> preparaciones;

    public BaseMenu() {
        //preparaciones=new ArrayList<>();
    }
    
    
    private synchronized static void crearInstancia() {
        if(instancia==null){
             instancia=new BaseMenu();
        
        }
    }
    
    public static BaseMenu getInstancia(){
        crearInstancia();
        
       return instancia;
    }
    
    
                 // SETTERS
    public void setPreparaciones(ArrayList<Preparacion> preparaciones) {
        this.preparaciones = preparaciones;
    }
    public void agregarPreparacion(String descripcion){
        Preparacion p=new Preparacion();
        p.setCod(preparaciones.size());
        p.setDescripcion(descripcion);
        preparaciones.add(p);
    }    
    
                 // GETTERS
    public ArrayList<Preparacion> getPreparaciones() {
        return preparaciones;
    }
    
    public String obtenerListado(){
        String listado="";
        
        for(Preparacion p : preparaciones){
           
            listado+="\n COD["+p.getCod()+"] "+p.getDescripcion()+" $"+p.getValor()+" PEDIDOS ["+p.getPedidos()+"]";
           
        }
        
        return listado;
    
    }
    
    public Preparacion obtenerPreparacion(int codigo){
        Preparacion preparacion=null;
        
        for(Preparacion p: preparaciones){
            if(p.getCod()==codigo){
                preparacion=p;
            }
        }
        
        
        return preparacion;
    }
    
    public boolean tienePrecio(Preparacion p){
        return p.getValor()!=0;
        
    }
    
    
}
