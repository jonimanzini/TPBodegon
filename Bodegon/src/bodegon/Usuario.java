
package bodegon;

import java.io.Serializable;


public abstract class Usuario implements Serializable {
    private String nombre;
    private String contraseña;
    private int codigo;

    public Usuario() {
    }
    
    
    
            //----------------------------------------------------SETTER
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
            //----------------------------------------------------GETTER
    public String getNombre() {
        return nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public int getCodigo() {
        return codigo;
    }
    
    @Override
    public String toString(){
        
        return "usuario: nombre{"+nombre+"}"+" contraseña:{"+contraseña+"}"+" codigo:{"+codigo+"}";
    }
    
    
    
    
    
    public void proceder(){}
    
}
