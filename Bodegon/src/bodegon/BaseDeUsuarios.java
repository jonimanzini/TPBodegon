
package bodegon;

import java.io.Serializable;
import java.util.ArrayList;


public class BaseDeUsuarios implements Serializable  {
       private static BaseDeUsuarios instancia;
       private ArrayList<Usuario> usuarios;

    public BaseDeUsuarios() {
    }
    
    
    private static void crearInstancia() {
        if(instancia==null){
             instancia=new BaseDeUsuarios();
        
        }
    }
    
    public static BaseDeUsuarios getInstancia(){
        crearInstancia();
        
       return instancia;
    }
    
    //SETTER
    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    
    public void agregarUsuario(Usuario u){
        u.setCodigo(usuarios.size());
        EntradaSalida.mostrarString(u.toString());
        usuarios.add(u);
        
    
    }
    
    //GETTER
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }
    
    
    public String obtenerListadoUsuarios(){
        String listado="";
        
        for(Usuario u : usuarios){
           listado+="\n"+u.toString();
           
        }
        
        return listado;
    }
    
    
    
}
