
package bodegon;

import java.io.Serializable;


public class UsuariosFactory  implements Serializable {
        private static UsuariosFactory f=null;
    
    private UsuariosFactory(){
    }
    
    public static UsuariosFactory getInstancia(){
        if(f==null){
            f=new UsuariosFactory();
        }
        return f;
    }
    
    public Usuario crearUsuario(String clase){
        Usuario u=null;
        try {       
            u = (Usuario) Class.forName(f.getClass().getPackage().getName()+"."+clase).newInstance();
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex)
        {       
            System.err.println(ex);   
        } 
         if (u == null) {
            throw new IllegalArgumentException(clase);
       
        }
        else{
             u.setNombre(EntradaSalida.leerString("INGRESAR NOMBRE"));
             u.setContraseña(EntradaSalida.leerPassword("INGRESAR CONTRA"));
        }
        
        
        return u; 
    }
    
    
}
